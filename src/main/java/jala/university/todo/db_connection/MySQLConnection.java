package jala.university.todo.db_connection;

import java.sql.Connection;
import java.sql.DriverManager;

public class MySQLConnection implements DatabaseConnection<Connection> {

    @Override
    public Connection getConnection(String dbName) {
        Connection connection = null;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String dbURL = "jdbc:mysql://localhost:3306/" + dbName;
            String dbUsername = "root";
            String dbPassword = "root";
            connection = DriverManager.getConnection(dbURL, dbUsername, dbPassword);
            System.out.println("Connection successful");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return connection;
    }
}
