package jala.university.todo.controllers;

import jala.university.todo.Main;
import jala.university.todo.utils.MessageUpdater;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import static jala.university.todo.Main.changeScene;

public class SignIn {
    public static String idUser;
    private static Connection connection;

    @FXML
    private TextField passwordTextField, emailTextField;
    @FXML
    private Text txtMessage;

    public void clear() {
        passwordTextField.clear();
        emailTextField.clear();
    }

    public void signUp(ActionEvent e) throws IOException {
        changeScene("/views/SignUp.fxml", "Sign Up", e);
    }

    public void onSignInButtonClick(ActionEvent actionEvent) throws SQLException, IOException {
        if (emailTextField.getText().isEmpty() || passwordTextField.getText().isEmpty()) {
            setTxtMessage("Please complete all fields.");
            return;
        }

        connection = Main.getConnection();
        String email = emailTextField.getText();
        String password = passwordTextField.getText();
        boolean bloqueo = bloqueoUser(email, MD5(password));

        String sql = "{CALL login(?, ?, ?)}";
        CallableStatement stmt = connection.prepareCall(sql);
        stmt.setString(1, email);
        stmt.setString(2, MD5(password));
        stmt.registerOutParameter(3, Types.VARCHAR);
        stmt.execute();

        String mensaje = stmt.getString(3);

        if (bloqueo) {
            setTxtMessage("Your account is temporarily blocked. Please try again in 2 hours.");
            return;
        }

        if ("Bienvenido".equals(mensaje)) {
            idUser = email;
            intoApp(actionEvent);
        } else {
            setTxtMessage(mensaje);
        }
    }

    private boolean bloqueoUser(String email, String contraseña) throws SQLException {
        String sql = "{CALL Bloqueo(?, ?, ?)}";
        CallableStatement stmt = connection.prepareCall(sql);
        stmt.setString(1, email);
        stmt.setString(2, contraseña);
        stmt.registerOutParameter(3, Types.BOOLEAN);
        stmt.execute();
        boolean estado = stmt.getBoolean(3);
        return estado;
    }

    private void intoApp(ActionEvent event) throws IOException {
        changeScene("/views/Dashboard.fxml", "Dashboard", event);
    }

    private String MD5(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(password.getBytes());
            BigInteger no = new BigInteger(1, messageDigest);
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public void setTxtMessage(String message) {
        txtMessage.setText(message);
        MessageUpdater messageUpdater = new MessageUpdater();
        messageUpdater.setTimeline(txtMessage);
    }


}