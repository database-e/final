create database to_do;
-- drop database to_do;
use to_do;
create table userT(
email varchar(100) primary key,
nameU varchar(200),
lastnameU varchar(200),
passwordU varchar(32) not null,
statusU boolean default (true),
registration_date date);
select * from userT;

create table userTBackUp(
email varchar(100) primary key,
nameU varchar(200),
lastnameU varchar(200),
passwordU varchar(32) not null,
statusU boolean,
registration_date date);

create table bloqueo_user(
    email varchar(50),
    Contador int,
    datatime datetime
);

create table category(
id int auto_increment primary key,
nameC varchar(30));

INSERT INTO category (nameC) VALUES ('Trabajo');
INSERT INTO category (nameC) VALUES ('Estudio');
INSERT INTO category (nameC) VALUES ('Casa');
INSERT INTO category (nameC) VALUES ('Personal');
INSERT INTO category (nameC) VALUES ('Importante');

create table task(
id int auto_increment,
statusT varchar(30),
id_category int,
primary key(id, id_category),
foreign key (id_category) references category(id),
nameT varchar(150) not null,
descripcion text,
expiration_date date,
creation_date date);
select * from task;

create table taskBackUp (
backup_id int auto_increment primary key,    
id int,
statusT varchar(30),
id_category int,
nameT varchar(150) not null,
descripcion text,
expiration_date date,
creation_date date
);

create table toDo(
email varchar(100),
id_task int,
primary key (email, id_task),
foreign key (email) references userT(email),
foreign key (id_task) references task(id)
);

create table toDoBackUp(
email varchar(100),
id_taskB int,
id_backup int auto_increment primary key);

-- TRIGGERS Y PROCEDIMIENTOS PARA EL USUARIO
DELIMITER //
DROP TRIGGER IF EXISTS user_trigger_backup//
CREATE TRIGGER user_trigger_backup
    BEFORE DELETE ON userT
    FOR EACH ROW
BEGIN
    DECLARE newEmail VARCHAR(100);
    DECLARE newNameU VARCHAR(200);
    DECLARE newLastnameU VARCHAR(200);
    DECLARE newPasswordU VARCHAR(32);
    DECLARE newStatusU BOOLEAN;
    DECLARE newRegistration_date DATE;
    SET newEmail = OLD.email;
    SET newNameU = OLD.nameU;
    SET newLastnameU = OLD.lastnameU;
    SET newPasswordU = OLD.passwordU;
    SET newStatusU = OLD.statusU;
    SET newRegistration_date = OLD.registration_date;
    INSERT INTO userTBackUp (email, nameU, lastnameU, passwordU, statusU, registration_date) 
    VALUES (newEmail, newNameU, newLastnameU, newPasswordU, newStatusU, newRegistration_date);
END //
DELIMITER ;

DELIMITER //
DROP PROCEDURE IF EXISTS register//
CREATE PROCEDURE register(
    IN newEmail VARCHAR(100),
    IN newNameU VARCHAR(200),
    IN newLastnameU VARCHAR(200),
    IN passwordU VARCHAR(32),
    IN statusU BOOLEAN,
    OUT mensaje VARCHAR(255)
)
BEGIN
    DECLARE user_existente INT;
    DECLARE registration_date DATE;
    
    SELECT COUNT(*) INTO user_existente FROM userT WHERE email = newEmail;

    IF user_existente = 0 THEN
        SET registration_date = NOW();
        INSERT INTO userT (email, nameU, lastnameU, passwordU, statusU, registration_date) 
        VALUES (newEmail, newNameU, newLastnameU, MD5(passwordU), statusU, registration_date);
        SET mensaje = 'Registrado correctamente';
    ELSE
        SET mensaje = 'El correo ya esta registrado';
    END IF;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS login;
DELIMITER //
CREATE PROCEDURE login(
    IN p_email VARCHAR(100),
    IN p_passwordU VARCHAR(32),
    OUT mensaje VARCHAR(255)
)
BEGIN
    DECLARE user_existente INT;
    DECLARE password_correcta INT;
    -- Existe usuario con el correo
    SELECT COUNT(*) INTO user_existente FROM userT WHERE email = p_email;
    IF user_existente > 0 THEN
        SELECT COUNT(*) INTO password_correcta FROM userT WHERE email = p_email AND passwordU = p_passwordU;
        IF password_correcta > 0 THEN
            SET mensaje = 'Bienvenido';
        ELSE
            SET mensaje = 'Contraseña incorrecta';
        END IF;
    ELSE
        SET mensaje = 'Usuario no registrado';
    END IF;
END //
DELIMITER ;

-- TIGGERS Y PROCEDIMIENTOS PARA LAS TAREAS
DELIMITER //
DROP TRIGGER IF EXISTS task_trigger_backup//
create trigger task_trigger_backup
    before delete on task
    for each row
begin
    declare id int;
    declare statusT varchar(30);
    declare id_category int;
    declare nameT varchar(150);
    declare descripcion text;
    declare expiration_date date;
    declare creation_date date;
    set id = old.id;
    set statusT = old.statusT;
    set id_category = old.id_category;
    set nameT = old.nameT;
    set descripcion = old.descripcion;
    set expiration_date = old.expiration_date;
    set creation_date = old.creation_date;
    INSERT INTO taskBackUp (id, statusT, id_category, nameT, descripcion, expiration_date, creation_date)
    VALUES (id, statusT, id_category, nameT, descripcion, expiration_date, creation_date);
end //
DELIMITER ;

DELIMITER //
DROP PROCEDURE IF EXISTS task_backup//
CREATE PROCEDURE task_backup(
    IN email VARCHAR(100),
    IN id_task INT,
    IN nameT VARCHAR(150)
)
BEGIN
    DECLARE task_deleted INT DEFAULT 0;
    DELETE t, td
    FROM toDo td
    INNER JOIN task t ON td.id_task = t.id
    WHERE td.id_task = id_task AND t.nameT = nameT;
    DELETE FROM task WHERE id = id_task AND nameT = nameT;
    SET task_deleted = ROW_COUNT();
	INSERT INTO toDoBackUp (email, id_taskB) VALUES (email, id_task);
END //
DELIMITER ;

DELIMITER //
DROP PROCEDURE IF EXISTS moveTaskFromBackup//
CREATE PROCEDURE moveTaskFromBackup(
    IN p_backupId INT,
    IN email VARCHAR(150),
    IN nameT VARCHAR(100)
)
BEGIN
    DECLARE task_Id INT;
    DECLARE task_exist INT;
    SELECT id INTO task_Id FROM taskBackUp WHERE backup_id = p_backupId;
    SELECT COUNT(*) INTO task_exist FROM task WHERE id = task_Id;
    IF task_exist = 0 THEN
        INSERT INTO task (id, statusT, id_category, nameT, descripcion, expiration_date, creation_date)
        SELECT id, statusT, id_category, nameT, descripcion, expiration_date, creation_date
        FROM taskBackUp
        WHERE backup_id = p_backupId;
        INSERT INTO toDo (email, id_task) VALUES (email, task_Id);
        DELETE td
        FROM toDoBackUp td
        INNER JOIN taskBackUp t ON td.id_taskB = t.id 
        WHERE td.id_taskB = task_Id AND t.nameT = nameT;
        DELETE FROM taskBackUp WHERE backup_id = p_backupId;
    END IF;
END //
DELIMITER ;

DELIMITER //
drop procedure if exists add_task//
CREATE PROCEDURE add_task(
    IN p_statusT VARCHAR(30),
    IN p_category INT,
    IN p_nameT VARCHAR(150),
    IN p_descripcion TEXT,
    IN p_expiration DATE,
    IN email VARCHAR(100),
    OUT mensaje BOOLEAN
)
BEGIN
    DECLARE task_exist int; 
    DECLARE id_task INT;
    SELECT COUNT(*) INTO task_exist FROM task WHERE nameT = p_nameT;
    IF task_exist =0 THEN
		INSERT INTO task (statusT, id_category, nameT, descripcion, expiration_date, creation_date)
		VALUES (p_statusT, p_category, p_nameT, p_descripcion, p_expiration, NOW());
		SET id_task = LAST_INSERT_ID();
		INSERT INTO toDo (email, id_task) VALUES (email, id_task);
        SET mensaje=true;
	ELSE 
		SET mensaje=false;
	END IF;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS Bloqueo;
DELIMITER //
CREATE PROCEDURE Bloqueo(
    IN p_email VARCHAR(100),
    IN p_password VARCHAR(100),
    OUT estado BOOLEAN
)
BEGIN
    DECLARE user_row_count INT;
    DECLARE user_attempts INT;
    DECLARE blocked_until DATETIME;
    DECLARE user_exists INT;
    SELECT COUNT(*) INTO user_row_count FROM userT WHERE email = p_email;
    IF user_row_count = 0 THEN
        SET estado = FALSE; -- Usuario no existe, por lo tanto, no está bloqueado
    ELSE
        SELECT COUNT(*) INTO user_exists FROM bloqueo_user WHERE email = p_email;
        IF user_exists > 0 THEN
            SELECT Contador, datatime INTO user_attempts, blocked_until FROM bloqueo_user WHERE email = p_email;
        ELSE
            SET user_attempts = 0;
            SET blocked_until = NULL;
        END IF;

        IF p_password <> (SELECT passwordU FROM userT WHERE email = p_email) THEN
            SET user_attempts = user_attempts + 1;
        ELSE
            SET user_attempts = 0;
        END IF;
        IF user_attempts >= 4 THEN
            SET blocked_until = TIMESTAMPADD(HOUR, 2, NOW()); -- Bloquear durante 2 horas
            SET user_attempts = 0;
        END IF;
        IF NOW() < blocked_until THEN
            SET estado = TRUE; -- Usuario está bloqueado
        ELSE
            SET estado = FALSE; -- Usuario no está bloqueado
        END IF;
        IF user_exists = 0 THEN
            INSERT INTO bloqueo_user (email, Contador, datatime) VALUES (p_email, user_attempts, blocked_until);
        ELSE
            UPDATE bloqueo_user SET Contador = user_attempts, datatime = blocked_until WHERE email = p_email;
        END IF;
    END IF;
END //
DELIMITER ;

-- Vistas
create view viewAllTask as SELECT u.nameU, t.id, t.nameT, t.descripcion, c.nameC AS category_name, t.statusT,t.expiration_date, t.creation_date,td.email
FROM task t JOIN toDo td ON t.id = td.id_task JOIN userT u ON td.email = u.email JOIN category c ON t.id_category = c.id;

create view trashTask as SELECT t.backup_id, u.nameU, t.id, t.nameT, t.descripcion, c.nameC AS category_name, t.statusT, t.expiration_date, t.creation_date,td.email 
FROM taskBackUp t JOIN toDoBackUp td ON t.id = td.id_taskB JOIN userT u ON td.email = u.email JOIN category c ON t.id_category = c.id;

create view importantTask as SELECT u.nameU, t.id, t.nameT, t.descripcion, c.nameC AS category_name, t.statusT, t.expiration_date, t.creation_date, td.email FROM task t 
JOIN toDo td ON t.id = td.id_task JOIN userT u ON td.email = u.email JOIN category c ON t.id_category = c.id;

CREATE INDEX idx_email ON userT(email);
CREATE INDEX idx_id_category ON task(id_category);
CREATE INDEX idx_dead_line ON task(expiration_date);
CREATE INDEX idx_name ON task(nameT);
