module jala.university.todo {
    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.controls;
    requires java.sql;
    requires mysql.connector.j;
    requires org.hibernate.orm.core;
    requires jakarta.persistence;
    requires java.naming;
    requires java.desktop;

    opens  jala.university.todo to org.hibernate.orm.core;
    opens jala.university.todo.controllers to javafx.fxml;
    exports jala.university.todo;
    exports jala.university.todo.controllers;
    exports jala.university.todo.tables;
    opens jala.university.todo.tables to org.hibernate.orm.core;

}
