package jala.university.todo.controllers;

import jala.university.todo.tables.Task;
import jala.university.todo.utils.MessageUpdater;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import static jala.university.todo.Main.changeScene;
import static jala.university.todo.Main.getConnection;

public class Dashboard implements Initializable {

    private static final Connection connection = getConnection();
    Task task = Task.getInstance();
    ObservableList<Task> tasksObservableList = FXCollections.observableArrayList();
    private boolean taskSelected = false;

    @FXML
    private TableColumn<Task, String> id, titleC, descriptionC, categoryC, statusC, creationC, deadC;
    @FXML
    private TableView<Task> searchTable;
    @FXML
    private TextField searchBar;
    @FXML
    private Text printName;
    @FXML
    private Text txtMessage;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {

            String nameQuery = "SELECT * FROM userT WHERE email = ?";
            PreparedStatement firstStament = connection.prepareStatement(nameQuery);
            firstStament.setString(1, SignIn.idUser);
            ResultSet firstResultSet = firstStament.executeQuery();
            if (firstResultSet.next()) {
                String name = firstResultSet.getString("nameU");
                printName.setText("Hi " + name + ",");
            }

            String taskQuery = "SELECT * FROM viewAllTask WHERE email = ?";
            PreparedStatement secondStatement = connection.prepareStatement(taskQuery);
            secondStatement.setString(1, SignIn.idUser);
            ResultSet secondResultSet = secondStatement.executeQuery();

            while (secondResultSet.next()) {

                String name = secondResultSet.getString("nameU");
                int id = secondResultSet.getInt("id");
                String nameT = secondResultSet.getString("nameT");
                String description = secondResultSet.getString("descripcion");
                String category = secondResultSet.getString("category_name");
                String status = secondResultSet.getString("statusT");
                String expiration_date = secondResultSet.getString("expiration_date");
                String creation_date = secondResultSet.getString("creation_date");
                //Populate table
                tasksObservableList.add(new Task(id, nameT, description, category, status, expiration_date, creation_date));
            }


            id.setCellValueFactory(new PropertyValueFactory<>("id"));
            titleC.setCellValueFactory(new PropertyValueFactory<>("nameT"));
            descriptionC.setCellValueFactory(new PropertyValueFactory<>("description"));
            categoryC.setCellValueFactory(new PropertyValueFactory<>("category"));
            statusC.setCellValueFactory(new PropertyValueFactory<>("status"));
            creationC.setCellValueFactory(new PropertyValueFactory<>("creation_date"));
            deadC.setCellValueFactory(new PropertyValueFactory<>("expiration_date"));
            searchTable.setItems(tasksObservableList);

            titleC.setResizable(false);
            descriptionC.setResizable(false);
            categoryC.setResizable(false);
            statusC.setResizable(false);
            creationC.setResizable(false);
            deadC.setResizable(false);


            FilteredList<Task> filteredList = new FilteredList<>(tasksObservableList, b -> true);
            searchBar.textProperty().addListener((observable, oldValue, newValue) -> {
                filteredList.setPredicate(task -> {
                    if (newValue == null || newValue.trim().isEmpty()) {
                        return true; // Mostrar todos
                    }

                    String searchKeyword = newValue.trim().toLowerCase();

                    return task.getNameT().toLowerCase().contains(searchKeyword) || task.getDescription().toLowerCase().contains(searchKeyword)
                            || task.getCategory().toLowerCase().contains(searchKeyword) || task.getStatus().toLowerCase().contains(searchKeyword)
                            || task.getCreation_date().toLowerCase().contains(searchKeyword) || task.getExpiration_date().toLowerCase().contains(searchKeyword);
                });
            });

            SortedList<Task> sortedList = new SortedList<>(filteredList);
            sortedList.comparatorProperty().bind(searchTable.comparatorProperty());
            searchTable.setItems(sortedList);
        } catch (SQLException ex) {
            Logger.getLogger(Dashboard.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }

        searchTable.setOnMouseClicked(event -> {
            if (event.getClickCount() == 1) {
                Task selectedTask = searchTable.getSelectionModel().getSelectedItem();
                if (selectedTask != null) {
                    taskSelected = true;
                    task.setId(selectedTask.getId());
                    task.setNameT(selectedTask.getNameT());
                    task.setDescription(selectedTask.getDescription());
                    task.setStatus(selectedTask.getStatus());
                    task.setCategory(selectedTask.getCategory());
                    task.setExpiration_date(selectedTask.getExpiration_date());
                }
            }
        });

    }

    @FXML
    private void exitSession(ActionEvent e) throws IOException {
        changeScene("/views/SignIn.fxml", "Exit", e);
    }

    @FXML
    private void addTask(ActionEvent e) throws IOException {
        changeScene("/views/AddTask.fxml", "Add task", e);
    }

    @FXML
    private void importantTaskView(ActionEvent e) throws IOException {
        changeScene("/views/ImportantTask.fxml", "Important task", e);
    }

    @FXML
    private void trashTaskView(ActionEvent e) throws IOException {
        changeScene("/views/TrashTask.fxml", "Trash task", e);
    }

    @FXML
    private void editTask(ActionEvent e) throws IOException {
        if (taskSelected) {
            changeScene("/views/EditTask.fxml", "Edit task", e);
        } else {
            txtMessage.setText("You must select a task to edit it.");
            MessageUpdater messageUpdater = new MessageUpdater();
            messageUpdater.setTimeline(txtMessage);
        }
    }
}
