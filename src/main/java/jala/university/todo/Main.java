package jala.university.todo;

import jala.university.todo.db_connection.MySQLConnection;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;

public class Main extends Application {

    private static Connection connection;

    public static void main(String[] args) {
        MySQLConnection database = new MySQLConnection();
        connection = database.getConnection("to_do");
        launch(args);
    }

    public static void changeScene(String resource, String print, ActionEvent e) throws IOException { //Metodo para llamar una nueva interfaz.
        FXMLLoader loader = new FXMLLoader(Main.class.getResource(resource));
        Parent root = loader.load();
        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        System.out.println(print);
    }

    public static Connection getConnection() {
        return connection;
    }

    @Override
    public void start(Stage Stage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/views/SignIn.fxml"));
            Scene scene = new Scene(root, 825, 525);
            Stage.setScene(scene);
            Stage.setResizable(false);
            Stage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
