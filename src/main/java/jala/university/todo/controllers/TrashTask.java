package jala.university.todo.controllers;

import jala.university.todo.tables.Task;
import jala.university.todo.utils.MessageUpdater;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import static jala.university.todo.Main.changeScene;
import static jala.university.todo.Main.getConnection;

public class TrashTask implements Initializable {

    private static final Connection connection = getConnection();
    Task task = Task.getInstance();
    ObservableList<Task> tasksObservableList = FXCollections.observableArrayList();
    int backUpTask = 0;
    private boolean taskSelected = false;
    @FXML
    private TableColumn<Task, String> id, titleC, descriptionC, categoryC, statusC, creationC, deadC;
    @FXML
    private TableView<Task> searchTable;
    @FXML
    private TextField searchBar;
    @FXML
    private Text printName;
    @FXML
    private Text txtMessage;

    @FXML
    public void addTask(ActionEvent e) throws IOException {
        changeScene("/views/AddTask.fxml", "Add task", e);
    }

    @FXML
    public void dashboardView(ActionEvent e) throws IOException {
        changeScene("/views/Dashboard.fxml", "Dashboard", e);
    }

    @FXML
    public void importantTaskView(ActionEvent e) throws IOException {
        changeScene("/views/ImportantTask.fxml", "Important Task", e);
    }

    @FXML
    public void exitSession(ActionEvent e) throws IOException {
        changeScene("/views/SignIn.fxml", "Exit", e);
    }

    @FXML
    private void returnTask(ActionEvent e) throws IOException {
        if (taskSelected) {
            sqlReturn();
            txtMessage.setText("The task was successfully restored");
            MessageUpdater messageUpdater = new MessageUpdater();
            messageUpdater.setTimeline(txtMessage);
            changeScene("/views/TrashTask.fxml", " ", e);
        } else {
            txtMessage.setText("You must select a task to restore it");
            MessageUpdater messageUpdater = new MessageUpdater();
            messageUpdater.setTimeline(txtMessage);
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {

            String nameQuery = "SELECT * FROM userT WHERE email = ?";
            PreparedStatement firstStament = connection.prepareStatement(nameQuery);
            firstStament.setString(1, SignIn.idUser);
            ResultSet firstResultSet = firstStament.executeQuery();
            if (firstResultSet.next()) {
                String name = firstResultSet.getString("nameU");
                printName.setText("Hi " + name + ",");
            }

            String query = "SELECT * FROM trashTask WHERE email = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, SignIn.idUser);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString("nameU");
                int id = resultSet.getInt("id");
                String nameT = resultSet.getString("nameT");
                String description = resultSet.getString("descripcion");
                String category = resultSet.getString("category_name");
                String status = resultSet.getString("statusT");
                String expiration_date = resultSet.getString("expiration_date");
                String creation_date = resultSet.getString("creation_date");
                //Populate table
                tasksObservableList.add(new Task(id, nameT, description, category, status, expiration_date, creation_date));
            }

            id.setCellValueFactory(new PropertyValueFactory<>("id"));
            titleC.setCellValueFactory(new PropertyValueFactory<>("nameT"));
            descriptionC.setCellValueFactory(new PropertyValueFactory<>("description"));
            categoryC.setCellValueFactory(new PropertyValueFactory<>("category"));
            statusC.setCellValueFactory(new PropertyValueFactory<>("status"));
            creationC.setCellValueFactory(new PropertyValueFactory<>("creation_date"));
            deadC.setCellValueFactory(new PropertyValueFactory<>("expiration_date"));
            searchTable.setItems(tasksObservableList);

            titleC.setResizable(false);
            descriptionC.setResizable(false);
            categoryC.setResizable(false);
            statusC.setResizable(false);
            creationC.setResizable(false);
            deadC.setResizable(false);
            FilteredList<Task> filteredList = new FilteredList<>(tasksObservableList, b -> true);
            searchBar.textProperty().addListener((observable, oldValue, newValue) -> {
                filteredList.setPredicate(task -> {
                    if (newValue == null || newValue.trim().isEmpty()) {
                        return true; // Mostrar todos
                    }
                    String searchKeyword = newValue.trim().toLowerCase();
                    return task.getNameT().toLowerCase().contains(searchKeyword) || task.getDescription().toLowerCase().contains(searchKeyword)
                            || task.getCategory().toLowerCase().contains(searchKeyword) || task.getStatus().toLowerCase().contains(searchKeyword)
                            || task.getCreation_date().toLowerCase().contains(searchKeyword) || task.getExpiration_date().toLowerCase().contains(searchKeyword);
                });
            });
            SortedList<Task> sortedList = new SortedList<>(filteredList);
            sortedList.comparatorProperty().bind(searchTable.comparatorProperty());
            searchTable.setItems(sortedList);
        } catch (SQLException ex) {
            Logger.getLogger(Dashboard.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }

        searchTable.setOnMouseClicked(event -> {
            if (event.getClickCount() == 1) {
                Task selectedTask = searchTable.getSelectionModel().getSelectedItem();
                if (selectedTask != null) {
                    taskSelected = true;
                    task.setId(selectedTask.getId());
                    task.setNameT(selectedTask.getNameT());
                    task.setDescription(selectedTask.getDescription());
                    task.setCategory(selectedTask.getCategory());
                    task.setExpiration_date(selectedTask.getExpiration_date());
                }
            }
        });
    }

    public void sqlReturn() {
        String sqlIdBackUp = "SELECT backup_id FROM taskBackUp WHERE id = ?";
        String sqlReturn = "CALL moveTaskFromBackup(?, ?,?)";
        try {
            // Obtener el backup_id de la tarea de respaldo
            PreparedStatement statement = connection.prepareStatement(sqlIdBackUp);
            statement.setInt(1, task.getId());
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                int backupId = resultSet.getInt("backup_id");
                PreparedStatement statement1 = connection.prepareStatement(sqlReturn);
                statement1.setInt(1, backupId);
                statement1.setString(2, SignIn.idUser);
                statement1.setString(3, task.getNameT());
                statement1.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
