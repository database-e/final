package jala.university.todo.utils;

import javafx.animation.KeyFrame;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class MessageUpdater {

    public void setTimeline(Text textLabel) {
        javafx.animation.Timeline timeline = new javafx.animation.Timeline(new KeyFrame(Duration.seconds(2), event -> {
            textLabel.setText("");
        }));
        timeline.play();
    }

}
