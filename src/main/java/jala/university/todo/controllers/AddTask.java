package jala.university.todo.controllers;

import jala.university.todo.Main;
import jala.university.todo.utils.MessageUpdater;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.io.IOException;
import java.sql.*;
import java.text.ParseException;
import java.time.LocalDate;

import static jala.university.todo.Main.changeScene;

public class AddTask {
    private static final Connection connection = Main.getConnection();
    @FXML
    private TextField title;
    @FXML
    private TextArea description;
    @FXML
    private DatePicker deadline;
    @FXML
    private ComboBox<String> categoriesComboBox;
    @FXML
    private Text txtMessage;


    @FXML
    private void addTask() throws SQLException, ParseException {
        if (title.getText().isEmpty()){
            setTxtMessage("Please, enter the title of the assignment.");
        } else if (categoriesComboBox.getValue() == null) {
            setTxtMessage("Please, select a category.");
        } else if (validationsDate()) {
            addTaskDataBase();
        }
    }

    @FXML
    public void initialize() throws SQLException {
        // Crear una lista observable de elementos para el ComboBox
        ObservableList<String> categoriesList = FXCollections.observableArrayList();
        PreparedStatement stmt = connection.prepareStatement("SELECT nameC FROM category");
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            String category = rs.getString("nameC");
            categoriesList.add(category);
        }
        categoriesComboBox.setItems(categoriesList);
    }

    public void addTaskDataBase() throws SQLException {
        String name = description.getText();
        if (title.getText().isEmpty()) {
            title.setText(" ");
        }
        String descriptionInput = title.getText();
        String deadlineInput = this.deadline.getValue().toString();
        String categoryInput = categoriesComboBox.getSelectionModel().getSelectedItem();
        String status = "Pendiente";
        PreparedStatement stmt1 = connection.prepareStatement("SELECT id FROM category WHERE nameC = ?");
        stmt1.setString(1, categoryInput);
        ResultSet rs1 = stmt1.executeQuery();
        int categoryId;
        if (rs1.next()) {
            categoryId = rs1.getInt("id");
        } else {
            categoryId = 0;
        }

        String sql = "{CALL add_task(?,?,?,?,?,?,?)}";
        CallableStatement stmt = connection.prepareCall(sql);
        stmt.setString(1, status);
        stmt.setString(2, String.valueOf(categoryId));
        stmt.setString(3, descriptionInput);
        stmt.setString(4, name);
        stmt.setString(5, deadlineInput);
        stmt.setString(6, SignIn.idUser);
        stmt.registerOutParameter(7, Types.BOOLEAN);
        stmt.executeUpdate();

        if (stmt.getBoolean(7)) {
            title.setText("");
            description.setText("");
            deadline.setValue(null);
            categoriesComboBox.setValue("");
            setTxtMessage("Task added.");

        } else {
            setTxtMessage("Task already exists.");
        }
    }

    @FXML
    private void returnPage(ActionEvent e) throws IOException {
        changeScene("/views/Dashboard.fxml", "Return Dashboard", e);
    }

    public boolean validationsDate() {
        LocalDate date = deadline.getValue();
        LocalDate now = LocalDate.now();
        if (date == null) {
            setTxtMessage("Please, enter a deadline.");
            return false;
        } else if (date.isBefore(now)) {
            setTxtMessage("Deadline can't be in the past.");
            return false;
        }
        return true;
    }

    public void setTxtMessage(String message) {
        txtMessage.setText(message);
        MessageUpdater messageUpdater = new MessageUpdater();
        messageUpdater.setTimeline(txtMessage);
    }
}