package jala.university.todo.db_connection;

public interface DatabaseConnection<T> {

    T getConnection(String dbName);

}
