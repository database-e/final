package jala.university.todo.controllers;

import jala.university.todo.Main;
import jala.university.todo.tables.Task;
import jala.university.todo.utils.MessageUpdater;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import static jala.university.todo.Main.changeScene;

public class EditTask {
    private static final Connection connection = Main.getConnection();
    private final String[] status = {"Pendiente", "Completado", "Cancelado"};
    Task task = Task.getInstance();
    @FXML
    private TextField title;
    @FXML
    private TextArea description;
    @FXML
    private DatePicker deadline;
    @FXML
    private ComboBox<String> categoriesComboBox, statusComboBox;
    @FXML
    private Text txtMessage;

    @FXML
    public void returnPage(ActionEvent e) throws IOException {
        changeScene("/views/Dashboard.fxml", "Return Dashboard", e);
    }

    public void initialize() {
        statusComboBox.getItems().addAll(status);
        try {
            ObservableList<String> categoriesList = FXCollections.observableArrayList();
            PreparedStatement stmt = connection.prepareStatement("SELECT nameC FROM category");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                String category = rs.getString("nameC");
                categoriesList.add(category);
            }
            // Establecer la lista de elementos en el ComboBox
            categoriesComboBox.setItems(categoriesList);
            title.setText(task.getNameT());
            description.setText(task.getDescription());
            if (task.getExpiration_date() != null) {
                deadline.setValue(LocalDate.parse(task.getExpiration_date()));
            }
            categoriesComboBox.setValue(task.getCategory());
            statusComboBox.setValue(task.getStatus());
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void saveTask() {
        if (title.getText().isEmpty() || deadline.isDisable()) {
            setTxtMessage("Nothing to save.");
        } else {
            title.setEditable(false);
            description.setEditable(false);
            deadline.setDisable(true);
            statusComboBox.setDisable(true);
            categoriesComboBox.setDisable(true);
            txtMessage.setText("");
            int id = task.getId();
            String fieldTitle = title.getText();
            String fieldDescription = description.getText();
            String fieldDeadline = "";
            String fieldStatus = statusComboBox.getValue();
            int fieldCategory = categoryId();
            if (deadline.getValue() != null) {
                fieldDeadline = deadline.getValue().toString();
            }
            try {
                if (validationsDate()) {
                    String query = "UPDATE task SET nameT = ?, descripcion = ?, expiration_date = ?, id_category = ?,statusT = ? WHERE id = ?";
                    PreparedStatement statement = connection.prepareStatement(query);
                    statement.setString(1, fieldTitle);
                    statement.setString(2, fieldDescription);
                    statement.setString(3, fieldDeadline);
                    statement.setInt(4, fieldCategory);
                    statement.setString(5, fieldStatus);
                    statement.setInt(6, id);
                    statement.executeUpdate();
                    statement.close();
                    setTxtMessage("Task update.");
                }


            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void editTask() {
        if (title.getText().isEmpty()) {
            setTxtMessage("Nothing to edit");
        } else {
            title.setEditable(true);
            description.setEditable(true);
            deadline.setDisable(false);
            statusComboBox.setDisable(false);
            categoriesComboBox.setDisable(false);
            txtMessage.setText(" ");
        }
    }

    public void deleteTask() {
        title.setEditable(false);
        description.setEditable(false);
        deadline.setDisable(true);
        int id = task.getId();
        String email = SignIn.idUser;
        if (title.getText().isEmpty()) {
            setTxtMessage("Nothing to delete.");
        } else {
            try {
                String query1 = "call task_backup (?,?,?)";
                PreparedStatement statement1 = connection.prepareStatement(query1);
                statement1.setString(1, email);
                statement1.setInt(2, id);
                statement1.setString(3, title.getText());
                statement1.executeUpdate();
                statement1.close();
                title.setText("");
                description.setText("");
                deadline.setValue(null);
                categoriesComboBox.setValue("");
                statusComboBox.setValue("");
                setTxtMessage("Task deleted");
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private int categoryId() {
        int categoryId = 0;
        String category = categoriesComboBox.getValue();
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT id FROM category WHERE nameC = ?");
            stmt.setString(1, category);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                categoryId = rs.getInt("id");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return categoryId;
    }

    public boolean validationsDate() {
        LocalDate date = deadline.getValue();
        LocalDate now = LocalDate.now();
        if (date.isBefore(now)) {
            setTxtMessage("Deadline can't be in the past.");
            return false;
        }
        return true;
    }

    public void setTxtMessage(String message) {
        txtMessage.setText(message);
        MessageUpdater messageUpdater = new MessageUpdater();
        messageUpdater.setTimeline(txtMessage);
    }
}

