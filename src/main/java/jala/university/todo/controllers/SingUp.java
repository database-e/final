package jala.university.todo.controllers;

import jala.university.todo.Main;
import jala.university.todo.utils.MessageUpdater;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.regex.Pattern;

import static jala.university.todo.Main.changeScene;

public class SingUp {

    private static Connection connection;
    @FXML
    private TextField nameTextField, emailTextField, lastNameTextField;
    @FXML
    private PasswordField passwordTextField, passwordConfirmTextField;
    @FXML
    private Text txtMessage;


    public void signIn(ActionEvent e) throws IOException {
        changeScene("/views/SignIn.fxml", "Sign In", e);
    }


    public void onSignUpButtonClick() {
        // Verificar si los campos están vacíos
        if (nameTextField.getText().isEmpty() || lastNameTextField.getText().isEmpty() ||
                emailTextField.getText().isEmpty() || passwordTextField.getText().isEmpty() ||
                passwordConfirmTextField.getText().isEmpty()) {
            setTxtMessage("All fields are required fields.");
            return;
        }

        // Resto del código para el registro del usuario
        connection = Main.getConnection();
        String name = nameTextField.getText();
        String lastName = lastNameTextField.getText();
        String email = emailTextField.getText();
        String password = passwordTextField.getText();
        String confirmPassword = passwordConfirmTextField.getText();
        String mensaje = "Usuario ingresado con exito";

        // Validar que el nombre y apellido no contengan números
        if (!isValidName(name)) {
            clear();
            setTxtMessage("The name must not contain numbers.");
            return;
        }
        if (!isValidName(lastName)) {
            clear();
            setTxtMessage("The surname must not contain numbers.");
            return;
        }
        // Validar el formato del correo electrónico
        if (!isValidEmail(email)) {
            clear();
            setTxtMessage("Use a valid e-mail address.");
            return;
        }
        if (!confirmPassword.equals(password)) {
            clear();
            setTxtMessage("Passwords do not match.");
            return;
        }


        // Insertar datos en la base de datos
        try {
            String sql = "{CALL register(?, ?, ?, ?, ?, ?)}";
            CallableStatement stmt = connection.prepareCall(sql);
            stmt.setString(1, email);
            stmt.setString(2, name);
            stmt.setString(3, lastName);
            stmt.setString(4, password);
            stmt.setBoolean(5, true); // Parámetro de salida
            stmt.setString(6, mensaje);
            stmt.executeUpdate();
            // Mostrar mensaje de éxito
            setTxtMessage("Success user successfully registered.");
            // Limpiar los campos después del registro exitoso
            clear();

        } catch (Exception e) {
            setTxtMessage("Invalid credentials.");
            e.printStackTrace();
            clear();
        }
    }

    // Método para validar el formato del correo electrónico
    private boolean isValidEmail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
        return Pattern.compile(emailRegex).matcher(email).matches();
    }

    public void clear() {
        nameTextField.clear();
        lastNameTextField.clear();
        emailTextField.clear();
        passwordTextField.clear();
        passwordConfirmTextField.clear();
    }

    // Método para validar que el nombre y apellido no contengan números
    private boolean isValidName(String name) {
        return !name.matches(".*\\d.*");
    }

    public void setTxtMessage(String message) {
        txtMessage.setText(message);
        MessageUpdater messageUpdater = new MessageUpdater();
        messageUpdater.setTimeline(txtMessage);
    }
}
