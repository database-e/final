package jala.university.todo.tables;

// Definición de la clase Task
public class Task {
    private static Task instance;
    private int id;
    private String nameT;
    private String description;
    private String category;
    private String status;
    private String expiration_date;
    private String creation_date;

    public Task() {

    }

    public Task(int id, String nameT, String description, String category, String status, String expiration_date, String creation_date) {
        this.id = id;
        this.nameT = nameT;
        this.description = description;
        this.category = category;
        this.status = status;
        this.expiration_date = expiration_date;
        this.creation_date = creation_date;
    }

    public static Task getInstance() {
        if (instance == null) {
            instance = new Task();
        }
        return instance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameT() {
        return nameT;
    }

    public void setNameT(String nameT) {
        this.nameT = nameT;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(String expiration_date) {
        this.expiration_date = expiration_date;
    }

    public String getCreation_date() {
        return creation_date;
    }
}
